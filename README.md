# BROADCAST MSG #

Please follow the steps below in order to use the JS library.

### STARTING POINT ###

```
#!javascript
<script src="http://cmfg.puca.com/ext/broadcast-msg-v1.min.js" type="text/javascript"></script>
```

```
#!javascript

var cm = new Cornmarket();
cm.init({
    data: 'QUOTE'
});
```
* data: QUOTE or RENEW

### GET DATA ###
```
#!javascript

cm.getData(function(params){
    //print params
});
```
### SEND DATA ###
The main property is **status**, but also it's possible to send extra parameters if it's needed.
```
#!javascript

cm.sendData({
     status: 'SUCCESS' 
})

```
* status: SUCCESS, FAILURE or DASHBOARD.
* SUCCESS: It will display a green/success popup message.
* FAILURE: It will display a red/failure popup message.
* DASHBOARD: It will redirect to the main page of the application.
 

### KEEP ALIVE ###
The following function can be implemented inside of a SetInterval function.


```
#!javascript

cm.keepAlive();
```