(function(){
    'use strict';
    var Cornmarket = function () {
        this.app = window.parent;
        this.getCallback = null;
        this.targetURL = "http://cmfg.puca.com";
        this.dataType = 'DEFAULT_SETTINGS';
        this.unSaveData = false;
    };
    Cornmarket.prototype.init = function (params) {
        var that= this,
            eventMethod = window.addEventListener ? "addEventListener" : "attachEvent",
            eventInstance = window[eventMethod],
            messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message",
            messageFn;

        if (params) {
            that.dataType = params.data ? params.data.toUpperCase() + '_SETTINGS' : 'DEFAULT_SETTINGS';
        }

        messageFn = function (event) {
            var origin = event.origin || event.originalEvent.origin;
            var obj, message, action;

            if (event.stopPropagation) {
                event.stopPropagation();
            }

            if (origin === that.targetURL) {
                try {
                    obj = JSON.parse(event.data);
                } catch (e) { }
                action = obj ? obj.action : event.data;

                message = obj ? obj.message : 'EMPTY';
                switch (action) {
                    case 'SETTINGS':
                        if (message) {
                            that.getCallback(message);
                        }
                        break;
                    case 'PROMPT':
                        that.sendPromptAlertFlag();
                        break
                    default:
                }
            }
        };
        window.removeEventListener(messageEvent, messageFn, false);
        eventInstance(messageEvent, messageFn, false);
    };
    Cornmarket.prototype.sendPostMessage = function (action, message) {
        var that = this;
        this.app.postMessage(JSON.stringify({
            action: action,
            message: message
        }), that.targetURL);
    };
    Cornmarket.prototype.sendData = function (object) {
        if (typeof object === "object") {
            this.sendPostMessage('SUCCESS', object);
        }
    };
    Cornmarket.prototype.getData = function (fn) {
        if (typeof fn === "function") {
            this.getCallback = fn;
            this.sendPostMessage('SETTINGS', this.dataType);
        }
    };
    Cornmarket.prototype.promptAlert = function (booleanFlag) {
        if (typeof booleanFlag === "boolean") {
            this.unSaveData = booleanFlag;
        }
    };
    Cornmarket.prototype.sendPromptAlertFlag = function () {
        this.sendPostMessage('PROMPT', {
            status: 'PROMPT',
            data: this.unSaveData
        });
    };
    Cornmarket.prototype.keepAlive = function () {
        this.sendPostMessage('ALIVE', {
            status: 'ALIVE'
        });
    };
    window.Cornmarket = Cornmarket;
}());